"use strict";

var fs         = require('fs'),

    express    = require('express'),
    app        = express();
    app.config = require('./environments/'+(process.env.NODE_ENV || 'development'));

// Подгружаем модели
{
  let models_path = __dirname + '/../app/models'
  fs.readdirSync(models_path).forEach(function (file) {
    if (~file.indexOf('.js')) require(models_path + '/' + file)
  });
}

// Подгружаем контроллеры
{
  let files_path = __dirname + '/../app/controllers'
  fs.readdirSync(files_path).forEach(function (file) { 
    if (~file.indexOf('.js')) {                                                                                    
      var controller = require(files_path + '/' + file); 
      controller.setup(app);                                                 
    }
  });
}

var mongoose   = require('mongoose');
require('./initializers/mongoose')(app, mongoose);

var passport   = require('passport');
require('./initializers/passport')(app, passport);

require('./initializers/express')(app, express, passport);

exports.app    = app;