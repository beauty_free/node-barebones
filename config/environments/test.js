"use strict";

//
// Testing config
//
module.exports = {
  server: {
    port: 4001,
    hostname: 'localhost',
  },
  database: {
    url: 'mongodb://localhost/express_test'
  }
};